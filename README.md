# envconfigR

## Introduction

This package mirrors behaviour of other languages' envconfig packages. The goal is to manage environment variables in a predefined struct.

## Installation

```
devtools::install_gitlab("kennylouie/envconfigR")
```

Alternatively, just source the R files

```
source("./R/envconfigR.R")
```

## Usage

```{R}
library("envconfigR")

Sys.setenv(DB_HOST = "psql", DB_PORT = 5432)
```

During instantiation of the envconfigR class, config contains the spec for the environment variables expected.

```{R}
env_names <- c("HOST", "PORT")
env_types <- c("character", "numeric")
env_defaults <- c("localhost", 5432)

envs <- envconfigR(config = list(env_names, env_types, env_defaults))
```
Results attribute contains the values of environment variables after the process() method.

The types and defaults also need to be specified.

```{R}
envs$process("DB")

envs$results[["HOST"]] # "psql"
envs$results[["PORT"]] # 5432
```

An optional param can be passed in to process(). It specifies a prefix on all the environment variables, e.g. DB_HOST
